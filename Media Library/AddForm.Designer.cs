﻿namespace Media_Library
{
    partial class AddForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TypeComboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.BookGroupBox = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.FilmGroupBox = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.FilmStatusComboBox = new System.Windows.Forms.ComboBox();
            this.FilmFormatComboBox = new System.Windows.Forms.ComboBox();
            this.ScreenFormatComboBox = new System.Windows.Forms.ComboBox();
            this.FilmRatingComboBox = new System.Windows.Forms.ComboBox();
            this.FilmGenreComboBox = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.FilmRunningTimeTextbox = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.FilmReleaseDateTextbox = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.FilmTitleTextbox = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.FilmStudioTextbox = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.StatusComboBox = new System.Windows.Forms.ComboBox();
            this.FormatComboBox = new System.Windows.Forms.ComboBox();
            this.AudienceComboBox = new System.Windows.Forms.ComboBox();
            this.GenreComboBox = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.PagesTextbox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.PublishDateTextbox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.PublisherTextbox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.TitleTextbox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.AuthorTextbox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.MusicGroupBox = new System.Windows.Forms.GroupBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.MusicStatusComboBox = new System.Windows.Forms.ComboBox();
            this.MusicFormatComboBox = new System.Windows.Forms.ComboBox();
            this.MusicGenreComboBox = new System.Windows.Forms.ComboBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.MusicNumberOfTracksTextbox = new System.Windows.Forms.TextBox();
            this.MusicReleaseDateTextbox = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.MusicTitleTextbox = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.MusicBandNameTextbox = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.MusicStudioTextbox = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.AddItemButton = new System.Windows.Forms.Button();
            this.BookGroupBox.SuspendLayout();
            this.FilmGroupBox.SuspendLayout();
            this.MusicGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // TypeComboBox
            // 
            this.TypeComboBox.FormattingEnabled = true;
            this.TypeComboBox.Items.AddRange(new object[] {
            "Film",
            "Book",
            "Music"});
            this.TypeComboBox.Location = new System.Drawing.Point(269, 44);
            this.TypeComboBox.Name = "TypeComboBox";
            this.TypeComboBox.Size = new System.Drawing.Size(121, 21);
            this.TypeComboBox.TabIndex = 1;
            this.TypeComboBox.SelectedIndexChanged += new System.EventHandler(this.TypeComboBox_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(177, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Type of Media :";
            // 
            // BookGroupBox
            // 
            this.BookGroupBox.Controls.Add(this.label10);
            this.BookGroupBox.Controls.Add(this.label9);
            this.BookGroupBox.Controls.Add(this.StatusComboBox);
            this.BookGroupBox.Controls.Add(this.FormatComboBox);
            this.BookGroupBox.Controls.Add(this.AudienceComboBox);
            this.BookGroupBox.Controls.Add(this.GenreComboBox);
            this.BookGroupBox.Controls.Add(this.label8);
            this.BookGroupBox.Controls.Add(this.label7);
            this.BookGroupBox.Controls.Add(this.PagesTextbox);
            this.BookGroupBox.Controls.Add(this.label6);
            this.BookGroupBox.Controls.Add(this.PublishDateTextbox);
            this.BookGroupBox.Controls.Add(this.label5);
            this.BookGroupBox.Controls.Add(this.PublisherTextbox);
            this.BookGroupBox.Controls.Add(this.label4);
            this.BookGroupBox.Controls.Add(this.TitleTextbox);
            this.BookGroupBox.Controls.Add(this.label3);
            this.BookGroupBox.Controls.Add(this.AuthorTextbox);
            this.BookGroupBox.Controls.Add(this.label2);
            this.BookGroupBox.Location = new System.Drawing.Point(12, 71);
            this.BookGroupBox.Name = "BookGroupBox";
            this.BookGroupBox.Size = new System.Drawing.Size(564, 160);
            this.BookGroupBox.TabIndex = 3;
            this.BookGroupBox.TabStop = false;
            this.BookGroupBox.Visible = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(302, 101);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(43, 13);
            this.label10.TabIndex = 2;
            this.label10.Text = "Status :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(302, 75);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(45, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "Format :";
            // 
            // FilmGroupBox
            // 
            this.FilmGroupBox.Controls.Add(this.label11);
            this.FilmGroupBox.Controls.Add(this.label12);
            this.FilmGroupBox.Controls.Add(this.FilmStatusComboBox);
            this.FilmGroupBox.Controls.Add(this.FilmFormatComboBox);
            this.FilmGroupBox.Controls.Add(this.ScreenFormatComboBox);
            this.FilmGroupBox.Controls.Add(this.FilmRatingComboBox);
            this.FilmGroupBox.Controls.Add(this.FilmGenreComboBox);
            this.FilmGroupBox.Controls.Add(this.label13);
            this.FilmGroupBox.Controls.Add(this.label14);
            this.FilmGroupBox.Controls.Add(this.label15);
            this.FilmGroupBox.Controls.Add(this.FilmRunningTimeTextbox);
            this.FilmGroupBox.Controls.Add(this.label16);
            this.FilmGroupBox.Controls.Add(this.FilmReleaseDateTextbox);
            this.FilmGroupBox.Controls.Add(this.label17);
            this.FilmGroupBox.Controls.Add(this.FilmTitleTextbox);
            this.FilmGroupBox.Controls.Add(this.label18);
            this.FilmGroupBox.Controls.Add(this.FilmStudioTextbox);
            this.FilmGroupBox.Controls.Add(this.label19);
            this.FilmGroupBox.Location = new System.Drawing.Point(12, 71);
            this.FilmGroupBox.Name = "FilmGroupBox";
            this.FilmGroupBox.Size = new System.Drawing.Size(564, 160);
            this.FilmGroupBox.TabIndex = 3;
            this.FilmGroupBox.TabStop = false;
            this.FilmGroupBox.Visible = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(278, 101);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(43, 13);
            this.label11.TabIndex = 2;
            this.label11.Text = "Status :";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(278, 75);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(45, 13);
            this.label12.TabIndex = 2;
            this.label12.Text = "Format :";
            // 
            // FilmStatusComboBox
            // 
            this.FilmStatusComboBox.FormattingEnabled = true;
            this.FilmStatusComboBox.Items.AddRange(new object[] {
            "Available",
            "Checked Out"});
            this.FilmStatusComboBox.Location = new System.Drawing.Point(366, 98);
            this.FilmStatusComboBox.Name = "FilmStatusComboBox";
            this.FilmStatusComboBox.Size = new System.Drawing.Size(169, 21);
            this.FilmStatusComboBox.TabIndex = 1;
            // 
            // FilmFormatComboBox
            // 
            this.FilmFormatComboBox.FormattingEnabled = true;
            this.FilmFormatComboBox.Items.AddRange(new object[] {
            "DVD",
            "Blu-Ray",
            "VHS",
            "Digital"});
            this.FilmFormatComboBox.Location = new System.Drawing.Point(366, 72);
            this.FilmFormatComboBox.Name = "FilmFormatComboBox";
            this.FilmFormatComboBox.Size = new System.Drawing.Size(169, 21);
            this.FilmFormatComboBox.TabIndex = 1;
            // 
            // ScreenFormatComboBox
            // 
            this.ScreenFormatComboBox.FormattingEnabled = true;
            this.ScreenFormatComboBox.Items.AddRange(new object[] {
            "Widescreen",
            "Fullscreen"});
            this.ScreenFormatComboBox.Location = new System.Drawing.Point(366, 46);
            this.ScreenFormatComboBox.Name = "ScreenFormatComboBox";
            this.ScreenFormatComboBox.Size = new System.Drawing.Size(169, 21);
            this.ScreenFormatComboBox.TabIndex = 1;
            // 
            // FilmRatingComboBox
            // 
            this.FilmRatingComboBox.FormattingEnabled = true;
            this.FilmRatingComboBox.Items.AddRange(new object[] {
            "G",
            "PG",
            "PG-13",
            "R",
            "NC-17",
            "X"});
            this.FilmRatingComboBox.Location = new System.Drawing.Point(90, 124);
            this.FilmRatingComboBox.Name = "FilmRatingComboBox";
            this.FilmRatingComboBox.Size = new System.Drawing.Size(169, 21);
            this.FilmRatingComboBox.TabIndex = 1;
            // 
            // FilmGenreComboBox
            // 
            this.FilmGenreComboBox.FormattingEnabled = true;
            this.FilmGenreComboBox.Items.AddRange(new object[] {
            "Action/Adventure",
            "Romance",
            "Comedy",
            "Science Fiction",
            "Fantasy",
            "Adult",
            "War",
            "Documentary",
            "Kids",
            "Family",
            "Young Adult"});
            this.FilmGenreComboBox.Location = new System.Drawing.Point(366, 20);
            this.FilmGenreComboBox.Name = "FilmGenreComboBox";
            this.FilmGenreComboBox.Size = new System.Drawing.Size(169, 21);
            this.FilmGenreComboBox.TabIndex = 1;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(278, 49);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(82, 13);
            this.label13.TabIndex = 2;
            this.label13.Text = "Screen Format :";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(278, 23);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(42, 13);
            this.label14.TabIndex = 2;
            this.label14.Text = "Genre :";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 127);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(44, 13);
            this.label15.TabIndex = 2;
            this.label15.Text = "Rating :";
            // 
            // FilmRunningTimeTextbox
            // 
            this.FilmRunningTimeTextbox.Location = new System.Drawing.Point(90, 98);
            this.FilmRunningTimeTextbox.Name = "FilmRunningTimeTextbox";
            this.FilmRunningTimeTextbox.Size = new System.Drawing.Size(169, 20);
            this.FilmRunningTimeTextbox.TabIndex = 3;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(6, 101);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(79, 13);
            this.label16.TabIndex = 2;
            this.label16.Text = "Running Time :";
            // 
            // FilmReleaseDateTextbox
            // 
            this.FilmReleaseDateTextbox.Location = new System.Drawing.Point(90, 72);
            this.FilmReleaseDateTextbox.Name = "FilmReleaseDateTextbox";
            this.FilmReleaseDateTextbox.Size = new System.Drawing.Size(169, 20);
            this.FilmReleaseDateTextbox.TabIndex = 3;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(6, 75);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(78, 13);
            this.label17.TabIndex = 2;
            this.label17.Text = "Release Date :";
            // 
            // FilmTitleTextbox
            // 
            this.FilmTitleTextbox.Location = new System.Drawing.Point(90, 46);
            this.FilmTitleTextbox.Name = "FilmTitleTextbox";
            this.FilmTitleTextbox.Size = new System.Drawing.Size(169, 20);
            this.FilmTitleTextbox.TabIndex = 3;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(6, 49);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(33, 13);
            this.label18.TabIndex = 2;
            this.label18.Text = "Title :";
            // 
            // FilmStudioTextbox
            // 
            this.FilmStudioTextbox.Location = new System.Drawing.Point(90, 20);
            this.FilmStudioTextbox.Name = "FilmStudioTextbox";
            this.FilmStudioTextbox.Size = new System.Drawing.Size(169, 20);
            this.FilmStudioTextbox.TabIndex = 3;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(6, 23);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(43, 13);
            this.label19.TabIndex = 2;
            this.label19.Text = "Studio :";
            // 
            // StatusComboBox
            // 
            this.StatusComboBox.FormattingEnabled = true;
            this.StatusComboBox.Items.AddRange(new object[] {
            "Available",
            "Checked Out"});
            this.StatusComboBox.Location = new System.Drawing.Point(366, 98);
            this.StatusComboBox.Name = "StatusComboBox";
            this.StatusComboBox.Size = new System.Drawing.Size(169, 21);
            this.StatusComboBox.TabIndex = 1;
            // 
            // FormatComboBox
            // 
            this.FormatComboBox.FormattingEnabled = true;
            this.FormatComboBox.Items.AddRange(new object[] {
            "Hardcover",
            "Softcover",
            "Audio"});
            this.FormatComboBox.Location = new System.Drawing.Point(366, 72);
            this.FormatComboBox.Name = "FormatComboBox";
            this.FormatComboBox.Size = new System.Drawing.Size(169, 21);
            this.FormatComboBox.TabIndex = 1;
            // 
            // AudienceComboBox
            // 
            this.AudienceComboBox.FormattingEnabled = true;
            this.AudienceComboBox.Items.AddRange(new object[] {
            "Adult",
            "Young Adult",
            "Teen",
            "Children",
            "Toddler",
            "Infant"});
            this.AudienceComboBox.Location = new System.Drawing.Point(366, 46);
            this.AudienceComboBox.Name = "AudienceComboBox";
            this.AudienceComboBox.Size = new System.Drawing.Size(169, 21);
            this.AudienceComboBox.TabIndex = 1;
            // 
            // GenreComboBox
            // 
            this.GenreComboBox.FormattingEnabled = true;
            this.GenreComboBox.Items.AddRange(new object[] {
            "Biography",
            "Autobiography",
            "History",
            "Political",
            "Self-Help",
            "Crafts",
            "Games",
            "Music",
            "Technology",
            "Learning",
            "Romance",
            "Science Fiction",
            "Fantasy",
            "Adventure"});
            this.GenreComboBox.Location = new System.Drawing.Point(366, 20);
            this.GenreComboBox.Name = "GenreComboBox";
            this.GenreComboBox.Size = new System.Drawing.Size(169, 21);
            this.GenreComboBox.TabIndex = 1;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(302, 49);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(58, 13);
            this.label8.TabIndex = 2;
            this.label8.Text = "Audience :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(302, 23);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(42, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "Genre :";
            // 
            // PagesTextbox
            // 
            this.PagesTextbox.Location = new System.Drawing.Point(107, 124);
            this.PagesTextbox.Name = "PagesTextbox";
            this.PagesTextbox.Size = new System.Drawing.Size(169, 20);
            this.PagesTextbox.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 127);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(95, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "Number of Pages :";
            // 
            // PublishDateTextbox
            // 
            this.PublishDateTextbox.Location = new System.Drawing.Point(107, 98);
            this.PublishDateTextbox.Name = "PublishDateTextbox";
            this.PublishDateTextbox.Size = new System.Drawing.Size(169, 20);
            this.PublishDateTextbox.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 101);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(73, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Publish Date :";
            // 
            // PublisherTextbox
            // 
            this.PublisherTextbox.Location = new System.Drawing.Point(107, 72);
            this.PublisherTextbox.Name = "PublisherTextbox";
            this.PublisherTextbox.Size = new System.Drawing.Size(169, 20);
            this.PublisherTextbox.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 75);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Publisher :";
            // 
            // TitleTextbox
            // 
            this.TitleTextbox.Location = new System.Drawing.Point(107, 46);
            this.TitleTextbox.Name = "TitleTextbox";
            this.TitleTextbox.Size = new System.Drawing.Size(169, 20);
            this.TitleTextbox.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 49);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Title :";
            // 
            // AuthorTextbox
            // 
            this.AuthorTextbox.Location = new System.Drawing.Point(107, 20);
            this.AuthorTextbox.Name = "AuthorTextbox";
            this.AuthorTextbox.Size = new System.Drawing.Size(169, 20);
            this.AuthorTextbox.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Author :";
            // 
            // MusicGroupBox
            // 
            this.MusicGroupBox.Controls.Add(this.label20);
            this.MusicGroupBox.Controls.Add(this.label21);
            this.MusicGroupBox.Controls.Add(this.MusicStatusComboBox);
            this.MusicGroupBox.Controls.Add(this.MusicFormatComboBox);
            this.MusicGroupBox.Controls.Add(this.MusicGenreComboBox);
            this.MusicGroupBox.Controls.Add(this.label23);
            this.MusicGroupBox.Controls.Add(this.label24);
            this.MusicGroupBox.Controls.Add(this.MusicNumberOfTracksTextbox);
            this.MusicGroupBox.Controls.Add(this.MusicReleaseDateTextbox);
            this.MusicGroupBox.Controls.Add(this.label25);
            this.MusicGroupBox.Controls.Add(this.MusicTitleTextbox);
            this.MusicGroupBox.Controls.Add(this.label26);
            this.MusicGroupBox.Controls.Add(this.MusicBandNameTextbox);
            this.MusicGroupBox.Controls.Add(this.label27);
            this.MusicGroupBox.Controls.Add(this.MusicStudioTextbox);
            this.MusicGroupBox.Controls.Add(this.label28);
            this.MusicGroupBox.Location = new System.Drawing.Point(12, 71);
            this.MusicGroupBox.Name = "MusicGroupBox";
            this.MusicGroupBox.Size = new System.Drawing.Size(570, 160);
            this.MusicGroupBox.TabIndex = 3;
            this.MusicGroupBox.TabStop = false;
            this.MusicGroupBox.Visible = false;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(278, 101);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(43, 13);
            this.label20.TabIndex = 2;
            this.label20.Text = "Status :";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(278, 75);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(45, 13);
            this.label21.TabIndex = 2;
            this.label21.Text = "Format :";
            // 
            // MusicStatusComboBox
            // 
            this.MusicStatusComboBox.FormattingEnabled = true;
            this.MusicStatusComboBox.Items.AddRange(new object[] {
            "Available",
            "Checked Out"});
            this.MusicStatusComboBox.Location = new System.Drawing.Point(383, 98);
            this.MusicStatusComboBox.Name = "MusicStatusComboBox";
            this.MusicStatusComboBox.Size = new System.Drawing.Size(169, 21);
            this.MusicStatusComboBox.TabIndex = 1;
            // 
            // MusicFormatComboBox
            // 
            this.MusicFormatComboBox.FormattingEnabled = true;
            this.MusicFormatComboBox.Items.AddRange(new object[] {
            "CD",
            "Digital",
            "Cassette"});
            this.MusicFormatComboBox.Location = new System.Drawing.Point(383, 72);
            this.MusicFormatComboBox.Name = "MusicFormatComboBox";
            this.MusicFormatComboBox.Size = new System.Drawing.Size(169, 21);
            this.MusicFormatComboBox.TabIndex = 1;
            // 
            // MusicGenreComboBox
            // 
            this.MusicGenreComboBox.FormattingEnabled = true;
            this.MusicGenreComboBox.Items.AddRange(new object[] {
            "Rock",
            "Alternative",
            "Country",
            "Swing",
            "Jazz",
            "Indie",
            "Pop",
            "Dance",
            "Techno",
            "Worship",
            "Accapella",
            "Instrunmental",
            "Rap",
            "Blues",
            "Bluegrass"});
            this.MusicGenreComboBox.Location = new System.Drawing.Point(383, 46);
            this.MusicGenreComboBox.Name = "MusicGenreComboBox";
            this.MusicGenreComboBox.Size = new System.Drawing.Size(169, 21);
            this.MusicGenreComboBox.TabIndex = 1;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(278, 49);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(42, 13);
            this.label23.TabIndex = 2;
            this.label23.Text = "Genre :";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(279, 23);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(98, 13);
            this.label24.TabIndex = 2;
            this.label24.Text = "Number of Tracks :";
            // 
            // MusicNumberOfTracksTextbox
            // 
            this.MusicNumberOfTracksTextbox.Location = new System.Drawing.Point(383, 20);
            this.MusicNumberOfTracksTextbox.Name = "MusicNumberOfTracksTextbox";
            this.MusicNumberOfTracksTextbox.Size = new System.Drawing.Size(169, 20);
            this.MusicNumberOfTracksTextbox.TabIndex = 3;
            // 
            // MusicReleaseDateTextbox
            // 
            this.MusicReleaseDateTextbox.Location = new System.Drawing.Point(90, 98);
            this.MusicReleaseDateTextbox.Name = "MusicReleaseDateTextbox";
            this.MusicReleaseDateTextbox.Size = new System.Drawing.Size(169, 20);
            this.MusicReleaseDateTextbox.TabIndex = 3;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(6, 101);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(78, 13);
            this.label25.TabIndex = 2;
            this.label25.Text = "Release Date :";
            // 
            // MusicTitleTextbox
            // 
            this.MusicTitleTextbox.Location = new System.Drawing.Point(90, 72);
            this.MusicTitleTextbox.Name = "MusicTitleTextbox";
            this.MusicTitleTextbox.Size = new System.Drawing.Size(169, 20);
            this.MusicTitleTextbox.TabIndex = 3;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(6, 75);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(33, 13);
            this.label26.TabIndex = 2;
            this.label26.Text = "Title :";
            // 
            // MusicBandNameTextbox
            // 
            this.MusicBandNameTextbox.Location = new System.Drawing.Point(90, 46);
            this.MusicBandNameTextbox.Name = "MusicBandNameTextbox";
            this.MusicBandNameTextbox.Size = new System.Drawing.Size(169, 20);
            this.MusicBandNameTextbox.TabIndex = 3;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(6, 49);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(69, 13);
            this.label27.TabIndex = 2;
            this.label27.Text = "Band Name :";
            // 
            // MusicStudioTextbox
            // 
            this.MusicStudioTextbox.Location = new System.Drawing.Point(90, 20);
            this.MusicStudioTextbox.Name = "MusicStudioTextbox";
            this.MusicStudioTextbox.Size = new System.Drawing.Size(169, 20);
            this.MusicStudioTextbox.TabIndex = 3;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(6, 23);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(43, 13);
            this.label28.TabIndex = 2;
            this.label28.Text = "Studio :";
            // 
            // AddItemButton
            // 
            this.AddItemButton.Location = new System.Drawing.Point(245, 309);
            this.AddItemButton.Name = "AddItemButton";
            this.AddItemButton.Size = new System.Drawing.Size(90, 33);
            this.AddItemButton.TabIndex = 4;
            this.AddItemButton.Text = "Add Item";
            this.AddItemButton.UseVisualStyleBackColor = true;
            this.AddItemButton.Visible = false;
            this.AddItemButton.Click += new System.EventHandler(this.AddItemButton_Click);
            // 
            // AddForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 720);
            this.Controls.Add(this.AddItemButton);
            this.Controls.Add(this.BookGroupBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TypeComboBox);
            this.Controls.Add(this.MusicGroupBox);
            this.Controls.Add(this.FilmGroupBox);
            this.Name = "AddForm";
            this.Text = "AddForm";
            this.Controls.SetChildIndex(this.FilmGroupBox, 0);
            this.Controls.SetChildIndex(this.MusicGroupBox, 0);
            this.Controls.SetChildIndex(this.TypeComboBox, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.BookGroupBox, 0);
            this.Controls.SetChildIndex(this.AddItemButton, 0);
            this.BookGroupBox.ResumeLayout(false);
            this.BookGroupBox.PerformLayout();
            this.FilmGroupBox.ResumeLayout(false);
            this.FilmGroupBox.PerformLayout();
            this.MusicGroupBox.ResumeLayout(false);
            this.MusicGroupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox TypeComboBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox BookGroupBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox StatusComboBox;
        private System.Windows.Forms.ComboBox FormatComboBox;
        private System.Windows.Forms.ComboBox AudienceComboBox;
        private System.Windows.Forms.ComboBox GenreComboBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox PagesTextbox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox PublishDateTextbox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox PublisherTextbox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox TitleTextbox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox AuthorTextbox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button AddItemButton;
        private System.Windows.Forms.GroupBox FilmGroupBox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox FilmStatusComboBox;
        private System.Windows.Forms.ComboBox FilmFormatComboBox;
        private System.Windows.Forms.ComboBox ScreenFormatComboBox;
        private System.Windows.Forms.ComboBox FilmRatingComboBox;
        private System.Windows.Forms.ComboBox FilmGenreComboBox;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox FilmRunningTimeTextbox;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox FilmReleaseDateTextbox;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox FilmTitleTextbox;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox FilmStudioTextbox;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.GroupBox MusicGroupBox;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.ComboBox MusicStatusComboBox;
        private System.Windows.Forms.ComboBox MusicFormatComboBox;
        private System.Windows.Forms.ComboBox MusicGenreComboBox;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox MusicNumberOfTracksTextbox;
        private System.Windows.Forms.TextBox MusicReleaseDateTextbox;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox MusicTitleTextbox;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox MusicBandNameTextbox;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox MusicStudioTextbox;
        private System.Windows.Forms.Label label28;
    }
}