﻿using Media_Library.Database;
using System;
using System.Windows.Forms;

namespace Media_Library
{
    public partial class AddForm : BaseForm
    {
        private MainForm mainForm;


        public AddForm()
        {
            InitializeComponent();
        }

        public AddForm(MainForm mainForm)
        {
            this.mainForm = mainForm;
            InitializeComponent();
           
        }
        protected override void mainMenuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mainForm.Show();
            this.Hide();
        }

        protected override void addItemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("You are already on the Add Item Screen.");
        }

        protected override void removeItemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RemoveForm form = new RemoveForm(mainForm);
            form.Show();
            Close();
        }

        protected override void lookupItemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LookupForm form = new LookupForm(mainForm);
            form.Show();
            Close();
        }

        protected override void updateItemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UpdateForm form = new UpdateForm(mainForm);
            form.Show();
            Close();
        }

        // Need to call groupbox SendToBack() method in order for the group box to be 
        // displayed when it is selected from the drop down menu (typecombobox)
        private void TypeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            string type = TypeComboBox.Text;
            AddItemButton.Visible = true;
            if (TypeComboBox.Text == "Film")
            {                               
                FilmGroupBox.SendToBack();
                FilmGroupBox.Visible = true;
                BookGroupBox.Visible = false;
                MusicGroupBox.Visible = false;
            }
            else if (type == "Book")
            {
                BookGroupBox.SendToBack();
                FilmGroupBox.Visible = false;
                BookGroupBox.Visible = true;
                MusicGroupBox.Visible = false;
            }
            else if (type == "Music")
            {
                MusicGroupBox.SendToBack();
                FilmGroupBox.Visible = false;
                BookGroupBox.Visible = false;
                MusicGroupBox.Visible = true;
            }
        }

        private void AddItemButton_Click(object sender, EventArgs e)
        {
            // Add item to the database
            if (FilmGroupBox.Visible)
            {
                AddFilm();
            }
            else if (BookGroupBox.Visible)
            {
                AddBook();
            }
            else if (MusicGroupBox.Visible)
            {
                AddMusic();
            }
        }

        // add a music item to the music database
        private void AddMusic()
        {
            using (var context = new MediaContext())
            {
                var music = new Music()
                {
                    Studio = MusicStudioTextbox.Text,
                    BandName = MusicBandNameTextbox.Text,
                    ReleaseDate = MusicReleaseDateTextbox.Text,
                    NumberOfTracks = MusicNumberOfTracksTextbox.Text,
                    Genre = MusicGenreComboBox.Text,
                    Format = MusicFormatComboBox.Text,
                    Status = MusicStatusComboBox.Text
                };
                context._Music.Add(music);
                context.SaveChanges();
            }
        }
        // add a book item to the book database
        private void AddBook()
        {
            using (var context = new MediaContext())
            {
                var book = new Book()
                {
                    Author = AuthorTextbox.Text,
                    Title = TitleTextbox.Text,
                    Publisher = PublisherTextbox.Text,
                    PublishDate = PublishDateTextbox.Text,
                    NumberOfPages = PagesTextbox.Text,
                    Genre = GenreComboBox.Text,
                    Audience = AudienceComboBox.Text,
                    Format = FormatComboBox.Text,
                    Status = StatusComboBox.Text
                };
                context._Books.Add(book);
                context.SaveChanges();
            }
        }
        // add a film item to the film database
        private void AddFilm()
        {
            using (var context = new MediaContext())
            {
                var film = new Film()
                {
                    Studio = FilmStudioTextbox.Text,
                    Title = FilmTitleTextbox.Text,
                    ReleaseDate = FilmReleaseDateTextbox.Text,
                    RunningTime = FilmRunningTimeTextbox.Text,
                    Rating = FilmRatingComboBox.Text,
                    Genre = FilmGenreComboBox.Text,
                    Format = FilmFormatComboBox.Text,
                    ScreenFormat = ScreenFormatComboBox.Text,
                    Status = FilmStatusComboBox.Text
                };
                context._Films.Add(film);
                context.SaveChanges();
            }
        }
    }
           
 }


    

