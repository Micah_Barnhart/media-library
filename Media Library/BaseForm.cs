﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Media_Library
{
    public partial class BaseForm : Form
    {
        public BaseForm()
        {
            InitializeComponent();            
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        // virtual methods for menu strip 

        protected virtual void mainMenuToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

       protected virtual void addItemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        protected virtual void removeItemToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        protected virtual void lookupItemToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        protected virtual void updateItemToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
    }
}
