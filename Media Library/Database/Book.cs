﻿using System.ComponentModel.DataAnnotations;

namespace Media_Library.Database
{
    public class Book
    {
        [Key]
        public int BookID { get; set; }
        public string Author { get; set; }
        public string Title { get; set; }
        public string Publisher { get; set; }
        public string PublishDate { get; set; }
        public string NumberOfPages { get; set; }
        public string Genre { get; set; }
        public string Audience { get; set; }
        public string Format { get; set; }
        public string Status { get; set; }
    }
}
