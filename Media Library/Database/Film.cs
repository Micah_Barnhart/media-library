﻿using System.ComponentModel.DataAnnotations;

namespace Media_Library.Database
{
    public class Film
    {
        [Key]
        public int FilmID { get; set; }
        public string Studio { get; set; }
        public string Title { get; set; }
        public string ReleaseDate { get; set; }
        public string[] Cast { get; set; }
        public string RunningTime { get; set; }
        public string Rating { get; set; }
        public string Genre { get; set; }
        public string Format { get; set; }
        public string ScreenFormat { get; set; }
        public string Status { get; set; }
    }
}
