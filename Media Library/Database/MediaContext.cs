﻿using System.Data.Entity;

namespace Media_Library.Database
{
    public class MediaContext :DbContext
    {
        public DbSet<Film> _Films { get; set; }
        public DbSet<Music> _Music { get; set; }
        public DbSet<Book> _Books { get; set;  }
    }
}
