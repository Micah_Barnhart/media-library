﻿using System.ComponentModel.DataAnnotations;

namespace Media_Library.Database
{
    public class Music
    {
        [Key]
        public int MusicID { get; set; }
        public string Studio { get; set; }
        public string BandName { get; set; }
        public string ReleaseDate { get; set; }
        public string NumberOfTracks { get; set; }
        public string Genre { get; set; }        
        public string Format { get; set; }
        public string Status { get; set; }
    }
}
