﻿using System;
using System.Windows.Forms;

namespace Media_Library
{
    public partial class MainForm : BaseForm
    {
        public MainForm()
        {
            InitializeComponent();
        }
               
        protected override void mainMenuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("You are already on the Main Menu.");
        }

        protected override void addItemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddForm form = new AddForm(this);
            form.Show();
            Hide();
        }

        protected override void removeItemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RemoveForm form = new RemoveForm(this);
            form.Show();
            Hide();
        }

        protected override void lookupItemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LookupForm form = new LookupForm(this);
            form.Show();
            Hide();
        }

        protected override void updateItemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UpdateForm form = new UpdateForm(this);
            form.Show();
            Hide();
        }
    }
}
