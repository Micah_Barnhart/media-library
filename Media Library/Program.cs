﻿using Media_Library.Database;
using System;
using System.Windows.Forms;

namespace Media_Library
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            CreateDatabase();
            Application.Run(new MainForm());
        }

        private static void CreateDatabase()
        {
            using (var context = new MediaContext())
            {
                context.Database.CreateIfNotExists();
            }
        }
    }
}
